package com.zuitt.culminating;

import java.util.ArrayList;

public class Phonebook {
    // Instance variable
    private ArrayList<Contact> contacts;

    // Constructor
    public Phonebook() {
        contacts = new ArrayList<Contact>();
    }

    // Getter
    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    // Setter
    public void setContacts(Contact contact) {
        contacts.add(contact);

    }
}