package com.zuitt.culminating;

import java.util.ArrayList;

public class Contact {

    // Instance variables
    private String name;
    private String contactNumber;
    private String address;
    // Default constructor
    public Contact() {}

    // Parameterized constructor
    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }
    // Getter
    public String getName() {return this.name;}
    public String getContactNumber() {return this.contactNumber;}
    public String getAddress() {return this.address;}

    // Setter

    public void setName(String name) {this.name = name;}
    public void setContactNumber(String contactNumber) {this.contactNumber = contactNumber;}
    public void setAddress(String address) {this.address = address;}


}
