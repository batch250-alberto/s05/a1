import com.zuitt.culminating.*;

public class Main {
    public static void main(String[] args) {

        Contact contact1 = new Contact("Johnny", "909090", "Quezon City");
        Contact contact2 = new Contact("Jeff", "909090", "Manila");
        Contact contact3 = new Contact("Peter", null, "Makati");
        Contact contact4 = new Contact("Hans", "909090", null);

        Phonebook phonebook = new Phonebook();

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);
        phonebook.setContacts(contact3);
        phonebook.setContacts(contact4);




        if(phonebook.getContacts().isEmpty()){
            System.out.println("Phonebook has no contacts");
        }
        else{
            for(Contact contact: phonebook.getContacts()){
                System.out.println(contact.getName());
                System.out.println("---------------------");
                if(contact.getAddress() != null && contact.getContactNumber() != null){
                    System.out.println(contact.getName() + " has the following registered number: " + "\n" + contact.getContactNumber());
                    System.out.println(contact.getName() + " has the following address: " + "\n" + contact.getAddress());
                }
                else if(contact.getAddress() == null){
                    System.out.println(contact.getName() + " don't have contact number but has the following address: " + "\n" + contact.getAddress());
                }
                else if(contact.getContactNumber() == null){
                    System.out.println(contact.getName() + " don't have address but has the following registered number: " + "\n" + contact.getContactNumber());
                }
                else{
                    System.out.println(contact.getName() + " don't have any contact details!" );
                }
            }
        }
    }
}